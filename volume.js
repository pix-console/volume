/////////////////////////////////////////////////////////////////////////////////////////////
//
// volume
//
//    Module that performs volume operations.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Dependencies
//
/////////////////////////////////////////////////////////////////////////////////////////////
var cli =      require("cli");
var io =       require("io");
var ioVolume = require("io-volume");
var type =     require("type");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Module definition
//
/////////////////////////////////////////////////////////////////////////////////////////////
define(["pkx", "configuration"], function echo(pkx, configuration) {
    var options = {
    };

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // parseParameters
    //
    // Returns an options object from the given cli parameters array.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function parseParameters(parameters, options) {
        options = options || {};

        // create new cli interpreter
        var proc = new cli();
        proc.command("list", "Displays a list of available volumes.")
                .option("--persistent", "Shows volumes that are persistent, files are guaranteed to be saved permanently.")
                .option("--temporary",  "Shows volumes that are temporary, files are not guaranteed to be saved permanently.")
                .option("--fixed",      "Shows volumes that can not be removed from the local system.")
                .option("--removable",  "Shows volumes that can be removed from the local system.");

        // parse the parameters
        var p = proc.parse(parameters);
        if (p) {
            if (!p["--help"]) {
                if (p.list) {
                    if (p.list["--persistent"]) {
                        options.persistent = true;
                    }
                    if (p.list["--temporary"]) {
                        options.temporary = true;
                    }
                    if (p.list["--fixed"]) {
                        options.fixed = true;
                    }
                    if (p.list["--removable"]) {
                        options.removable = true;
                    }

                    if (!p.list["--help"]) {
                        printVolumeList();
                    }
                }
                else {
                    proc.parse(parameters.concat("--help"));
                }
            }
        }
    

        return options;
    }

    function getVolumeRootURI(volume, idx, callback) {
        var prom = volume.getURI("/");
        if (!prom.then) {
            console.error("Bad driver '" + volume.protocol + "': missing .getURI() or function does not return a promise.");
            return;
        }
        prom.then(function(uri) {
            callback(idx, uri);
        }).catch(function(e) {
            callback(idx, e);
        });
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // printVolumeList
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function printVolumeList() {
        var volumes = io.volumes.get();

        var rows = [];
        var count = 0;
        for (var v in volumes) {
            rows[v] = { "volume" : volumes[v] };
            try {
                getVolumeRootURI(volumes[v], v, function(idx, uri) {
                    count++;

                    if (uri instanceof Error) {

                    }
                    else {
                        rows[idx].root = uri;
                    }

                    if (count == volumes.length) {
                        var table = new cli.TextTable();
                        table.setHorizontalSpacing(6);
                        table.addRow(["ID", "Name", "Protocol", "Root"]);
                        table.addRow([]);
                        for (var r in rows) {
                            if ((options.persistent && rows[r].volume.class !== ioVolume.CLASS_PERSISTENT) || 
                                (options.temporary  && rows[r].volume.class !== ioVolume.CLASS_TEMPORARY) ||
                                (options.fixed      && rows[r].volume.type  !== ioVolume.TYPE_FIXED) ||
                                (options.removable  && rows[r].volume.type  !== ioVolume.TYPE_REMOVABLE)) {
                                continue;
                            }
                            table.addRow([rows[r].volume.id, rows[r].volume.name, rows[r].volume.protocol, rows[r].root]);
                        }

                        console.log("\nAvailable volumes:\n \n");

                        table.print();

                        console.log(" ");
                    }
                });
            }
            catch(e) {
                console.error(e);
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // main
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (!configuration || !type.isArray(configuration.argv)) {
        throw new Error("This is a console app and can only be used from the console.");
    }

    parseParameters(configuration.argv, options);    
});
